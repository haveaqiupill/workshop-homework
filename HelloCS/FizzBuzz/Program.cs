﻿using System;
using System.IO;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            String filename = args[0];
            String x = args[1];
            String y = args[2];
            Process(filename, x, y);
        }
        
        static void Process(String filename, String x, String y)
        {
            String[] text = File.ReadAllLines(filename);
            for (long i = 0; i < text.Length; i++)
            {
                String[] temp = text[i].Split(',');
                String toPrint = "";
                for (long j = Int64.Parse(temp[0]); j <= Int64.Parse(temp[1]); j++)
                {
                    if (j % 3 == 0 && j % 5 == 0)
                    {
                        toPrint += x + y + " ";
                    }
                    else if (j % 3 == 0)
                    {
                        toPrint += x + " ";
                    }
                    else if (j % 5 == 0)
                    {
                        toPrint += y + " ";
                    }
                    else
                    {
                        toPrint += j + " ";
                    }
                }
                Console.WriteLine(toPrint);
            }
        }
    }
}
