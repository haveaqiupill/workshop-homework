﻿using System;
using System.IO;

namespace Text_Wrap
{
    class Program
    {
        static void Main(string[] args)
        {
            String filename = args[0];
            long wordLimit = Int64.Parse(args[1]);
            var text = File.ReadAllText(filename);
            Process(text, wordLimit);
        }

        static void Process(String text, long wordLimit)
        {
            String[] splitted = text.Split(null);
            for (long i = 1; i <= splitted.Length; i++)
            {
                if (i % wordLimit == 0)
                {
                    Console.WriteLine(splitted[i - 1]);
                }
                else
                {
                    Console.Write(splitted[i-1] + " ");
                }
            }
        }
    }
}
