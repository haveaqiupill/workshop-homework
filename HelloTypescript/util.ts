function Sum(input: number[]): number {
    return input.reduce((a, b) => a + b);
}

function SumOfDigits(input: number): number {
    const digits = input.toString().split('').map(e => parseInt(e));
    return Sum(digits);
}

export {SumOfDigits}