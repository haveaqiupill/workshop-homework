interface Animal {

    EnglishCry(): string;

    JapaneseCry(): string;

    ChineseCry(): string;

}

export {Animal}