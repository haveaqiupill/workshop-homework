import * as fs from "fs";

const inputArray = process.argv.splice(2);
const fileName: string = inputArray[0];
const numFix: number = parseInt(inputArray[1]);

function wrapText(filename: string) {
    const txt = fs.readFileSync(filename, 'utf8');
    const splitted = txt.split(" ");

    for(let i:number = 0; i<splitted.length; i+=numFix) {
        let combined: string = '';
        for(let j:number = 0; j<numFix; j++) {
            if(i+j >= splitted.length) {
                break;
            }
            combined += splitted[i+j] + ' ';
        }
        console.log(combined);
    }
}

wrapText(fileName);


