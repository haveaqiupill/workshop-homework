import {FizzBuzzObject} from './FizzBuzzObject';

const inputArray = process.argv.splice(2);
const fileName: string = inputArray[0];
const x: string = inputArray[1];
const y: string = inputArray[2];

const z = new FizzBuzzObject(x, y, fileName);
z.process();

