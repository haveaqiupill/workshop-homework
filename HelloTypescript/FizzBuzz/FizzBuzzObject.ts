import * as fs from "fs";

export class FizzBuzzObject {

    x: string;
    y: string;
    filename : string;

    constructor(x: string, y:string, filename: string) {
        this.x = x;
        this.y = y;
        this.filename = filename;
    }

    process() {
        const txt = fs.readFileSync(this.filename, 'utf8');
        const splitted = txt.split("\n");
        for(let i:number = 0; i<splitted.length; i++) {
            const nums = splitted[i].split(",");
            const a: number = parseInt(nums[0]);
            const b: number = parseInt(nums[1]);
            let toPrint: string = "";

            for(let j:number = a; j<=b; j++) {
                if(j%3==0 && j%5==0) {
                    toPrint += this.x + this.y + " ";
                } else if(j%3==0) {
                    toPrint += this.x + " ";
                } else if(j%5==0) {
                    toPrint += this.y + " ";
                } else {
                    toPrint += j + " ";
                }
            }
            console.log(toPrint);
        }
    }


}