import {Animal} from "./animal";

class Cat implements Animal {


    ChineseCry(): string {
        return "mao";
    }

    EnglishCry(): string {
        return "meow";
    }

    JapaneseCry(): string {
        return "nyan";
    }
}

export {Cat}