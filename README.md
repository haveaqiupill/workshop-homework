# The Humble Programmer Workshop Homework
This repository consists of the completed homework for:
  * TypeScript Basics
  * C# Basics
  * Naive Database
  * Deployment

## TypeScript Basics
This is a simple exercise on how to set-up TypeScript environment and quick start on TypeScript, consisting of Text-Wrap and FizzBuzz Practice Solutions in TypeScript.

## C# Basics
This is a simple exercise on how to set-up .NET Core environment and quick start on C#, consisting of Text-Wrap and FizzBuzz Practice Solutions in C#.

## Naive Database

## Deployment